let titulo, descripcion, categoria, tiempo;
let objRecordatorios = [], timer = [], colors = ["#9BD1FD", "#FFC299", "#FE71AE", "#FFB81F", "#FF7F5C", "#0AC2B9", "#3F612D", "#CCBB00", "#665687", "#C41508"];
const CARACTERES = 'áéíóúÁÉÍÓÚabcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ.0123456789 ';

function Recordatorio(titulo, descripcion, categoria, tiempo) {
    this.titulo = titulo
    this.descripcion = descripcion
    this.categoria = categoria
    this.tiempo = tiempo
}

(function () {
    var urlRelativa = window.location.toString();
    if (urlRelativa.indexOf('#') != -1) {
        urlRelativa = urlRelativa.split('#');
        window.location = urlRelativa[0];
    }
    imprimirDiv();
    activarNotificaciones();
})();

function activarNotificaciones() {
    if (Notification.permission !== "granted") {
        Notification.requestPermission();
        new Noty({
            type: 'info',
            layout: 'topLeft',
            theme: 'mint',
            text: 'Active las notificaiones para obtener un mejor uso de la aplicación',
            timeout: 9000
        }).show();
    }
}

function imprimirDiv() {
    if (localStorage.getItem("record").toString() == "[]") {
        localStorage.setItem("record", JSON.stringify(objRecordatorios));
        mostrarVacio();
    } else {
        objRecordatorios = JSON.parse(localStorage.getItem("record"));
        mostrarRecordatorios();
    }
}

function notificacion(pos) {
    if (Notification) {
        if (Notification.permission !== "granted") {
            Notification.requestPermission()
        }
        var title = "¡Se acabó el tiempo!";
        var extra = {
            icon: "assets/img/favicon.png",
            body: `${objRecordatorios[pos].titulo} | ${objRecordatorios[pos].categoria}`
        }
        var noti = new Notification(title, extra)
        setTimeout(function () { noti.close() }, 10000)
    }
}

function mostrarVacio() {
    var recordDiv = document.getElementById("divRecord");
    recordDiv.innerHTML = ` <div class="card text-center">
    <div class="card-body" style="height: 16em ;">
      <h2 class="card-title">Opss..</h2> <br>
      <p class="card-text">No tienes recordatorios</p> <br>
      <br>
      <a href="#nuevo" class="btn btn-outline-primary font-weight-bold">¡Registrar uno!</a>
    </div>
  </div>`;
}

function mostrarRecordatorios() {
    document.getElementById("botonesGenerales").hidden = false;
    var recordDiv = document.getElementById("divRecord");
    recordDiv.innerHTML = `<div class="card" style="width: 100%;"> <ul class="list-group list-group-flush"> `;
    for (let index = 0; index < objRecordatorios.length; index++) {
        timer.push(new Timer());
        var rnd = Math.ceil(Math.random() * colors.length - 1);
        recordDiv.innerHTML += `
        <li class="list-group-item">
            <div class="form-row">
                <div class="col-sm-8">
                    <b>${objRecordatorios[index].titulo} | </b><b style="color: ${colors[rnd]}">${objRecordatorios[index].categoria}</b> <br>
                    ${objRecordatorios[index].descripcion}
                </div>
                <div class="col-sm-2">
                    <div id="tempS${index}">
                        <div class="values${index}" font-italic" style="font-size: 1.25em;">
                        </div>
                    </div>
                </div>
                <div class="col-sm-2" id="tempS${index}">           
                    <div class="btn-group btn-group-sm values" role="group" aria-label="Basic example">                  
                    <button type="button" class="startButton btn btn-success" id="btnId${index}" onclick="iniciar(${index})">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-play-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M11.596 8.697l-6.363 3.692c-.54.313-1.233-.066-1.233-.697V4.308c0-.63.692-1.01 1.233-.696l6.363 3.692a.802.802 0 0 1 0 1.393z"/>
                            </svg>
                        </button>                    
                        <button type="button" class="pauseButton btn btn-warning" onclick="pausar(${index})">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pause-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path d="M5.5 3.5A1.5 1.5 0 0 1 7 5v6a1.5 1.5 0 0 1-3 0V5a1.5 1.5 0 0 1 1.5-1.5zm5 0A1.5 1.5 0 0 1 12 5v6a1.5 1.5 0 0 1-3 0V5a1.5 1.5 0 0 1 1.5-1.5z"/>
                            </svg>
                        </button>
                        <button type="button" class="btn btn-danger" onclick="eliminarRecordatorio(${index})">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash-fill " fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5a.5.5 0 0 0-1 0v7a.5.5 0 0 0 1 0v-7z"/>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </li> `;
    }
    recordDiv.innerHTML += `</ul> </div>`;
}

function iniciar(pos) {
    try {
        var sentencia = `#tempS${pos} .values${pos}`;
        timer[pos].start({ countdown: true, startValues: { seconds: objRecordatorios[pos].tiempo } });
        $(sentencia).html(timer[pos].getTimeValues().toString());
        timer[pos].addEventListener('secondsUpdated', function (e) {
            $(sentencia).html(timer[pos].getTimeValues().toString());
        });
        timer[pos].addEventListener('targetAchieved', function (e) {  
            $(sentencia).html('¡Listo!'); 
            notificacion(pos);
        });         
    } catch (error) {
        console.log("Ya esta corriendo el tiempo");
    }
}

function pausar(pos) {
    timer[pos].pause();
}

function eliminarRecordatorio(indice) {
    Swal.fire({
        title: '¿Estás seguro de eliminarlo?',
        html: `<p> <b class="text-danger"> ${objRecordatorios[indice].titulo}</b> será quitado de tu lista.</p>`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminalo!',
        cancelButtonText: 'No, guardalo!',
    }).then((result) => {
        if (result.isConfirmed) {
            try {
                objRecordatorios.splice(indice, 1);
                timer.splice(indice, 1);
                new Noty({
                    type: 'success',
                    layout: 'topRight',
                    theme: 'mint',
                    text: 'El recordatorio se elimino correctamente.',
                    timeout: 2000
                }).show();
                localStorage.setItem("record", JSON.stringify(objRecordatorios));
                imprimirDiv();
            } catch (Exception) {
                console.log("Ocurrio un error al eliminar");
            }
        }
    })
}

function iniciarTodos() {
    for (let i = 0; i < objRecordatorios.length; i++) {
        iniciar(i);
    }
    document.getElementById("informe").innerHTML = "Tiempo corriendo"
}

function pausarTodos() {
    for (let i = 0; i < objRecordatorios.length; i++) {    
         pausar(i);       
    }
    document.getElementById("informe").innerHTML = "Pausados"
}

function eliminarTodos() {
    Swal.fire({
        title: '¿Estás seguro de eliminar todos los recordatorios?',
        text: 'Este cambio es irreversible',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminalos!',
        cancelButtonText: 'No, guardalos!',
    }).then((result) => {
        if (result.isConfirmed) {
            try {
                for (let i = 0; i < objRecordatorios.length; i++) {
                    objRecordatorios.splice(i);
                    timer.splice(i);
                }
                document.getElementById("informe").innerHTML = ""
                document.getElementById("botonesGenerales").hidden = true;
                new Noty({
                    type: 'success',
                    layout: 'topRight',
                    theme: 'mint',
                    text: 'El recordatorio se elimino correctamente.',
                    timeout: 2000
                }).show();
                localStorage.setItem("record", JSON.stringify(objRecordatorios));
                imprimirDiv();
            } catch (Exception) {
                console.log("Ocurrio un error al eliminar");
            }
        }
    })
}

async function agendarRecordatorio() {
    let campos = [await funTiempo(), await funCategoria(), await funTitulo(), await funDescripcion()];
    let validacion = campos[0] && campos[1] && campos[2] && campos[3];
    if (validacion) {
        new Noty({
            type: 'success',
            layout: 'topRight',
            theme: 'mint',
            text: 'El recordatorio se agrego correctamente.',
            timeout: 1500
        }).show();
        objRecordatorios.push(new Recordatorio(titulo, descripcion, categoria, tiempo));
        localStorage.setItem("record", JSON.stringify(objRecordatorios));
        timer.push(new Timer());
        document.getElementById("iForm").reset();
        mostrarRecordatorios();
    } else {
        new Noty({
            type: 'error',
            layout: 'topRight',
            theme: 'mint',
            text: 'Error, verifique que todos los campos esten correctos.',
            timeout: 1500
        }).show();
    }
}

function funDescripcion() {
    return new Promise(retornar => {
        let flag = true;
        let xDescripcion = document.getElementById('xDescripcion').value;
        descripcion = xDescripcion;
        retornar(flag);
    })
}

function funTitulo() {
    return new Promise(retornar => {
        let flag = false, validacion = true;
        let xTitulo = document.getElementById('xTitulo').value;
        if (xTitulo.length != 0) {
            for (let i = 0; i < xTitulo.length; i++) {
                if (CARACTERES.indexOf(xTitulo.charAt(i)) == -1) {
                    validacion = false;
                    break;
                }
            }
            if (validacion) {
                titulo = xTitulo;
                flag = true;
            } else {
                new Noty({
                    type: 'warning',
                    layout: 'topRight',
                    theme: 'mint',
                    text: 'El título del recordatorio no debe llevar caracteres especiales.',
                    timeout: 1500
                }).show();
            }
        } else {
            new Noty({
                type: 'warning',
                layout: 'topRight',
                theme: 'mint',
                text: 'El titulo del recordatorio no debe estar vacío.',
                timeout: 1500
            }).show();
        }
        retornar(flag);
    })
}

function funTiempo() {
    return new Promise(retornar => {
        let flag = false;
        let xTiempo = document.getElementById('xTiempo').value;
        if (xTiempo.indexOf("none") == -1) {
            tiempo = xTiempo * 60;
            flag = true;
        } else {
            new Noty({
                type: 'warning',
                layout: 'topRight',
                theme: 'mint',
                text: 'Debe seleccionar el tiempo de duración del recordatorio',
                timeout: 1500
            }).show();
            flag = false;
        }
        retornar(flag);
    })
}

function funCategoria() {
    return new Promise(retornar => {
        let flag = false;
        let xCategoria = document.getElementById('xCategoria').value;
        if (xCategoria.indexOf("none") == -1) {
            categoria = xCategoria;
            flag = true;
        } else {
            new Noty({
                type: 'warning',
                layout: 'topRight',
                theme: 'mint',
                text: 'Debe seleccionar la categoría del recordatorio',
                timeout: 1500
            }).show();
        }
        retornar(flag);
    })
}

function creditos() {
    Swal.fire({
        title: 'Integrantes ᕦ(ò_óˇ)ᕤ',
        html: ' Adame Najera Raúl Genaro<br>Millán Villarreal Violeta Jazmín<br>Saldaña Espinoza Hector<br>Vasquez Martinez Jair David',
        width: 500,
        padding: '3em',
        background: '#fff',
        backdrop: `
          rgba(0,0,123,0.4)
          url("/assets/img/nyan-cat.gif")
          left top
          no-repeat
        `
    })
}

